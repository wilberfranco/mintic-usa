"""
Reto 1: Deducción de impuestos a la nómina

Un empleado de una empresa dedicada a comercializar café desea conocer cuánto dinero de impuestos debe pagar en referencia a la nómina exigidos por la ley en relación con los pagos que la empresa le realizan mensualmente. Se ha firmado un contrato que le permite trabajar 48 horas semanales. Con el propósito de verificar el valor total de los descuentos han decidido construir un programa en Python que le permita verificar el valor de su salario antes y después de realizar los descuentos. Después de consultar sobre la normatividad y revisar con detalle su contrato laboral nota que debe tener en cuenta los siguientes aspectos:

El valor de una hora de trabajo normal se obtiene dividiendo el salario base sobre 199.
Las horas extras se liquidan con un recargo del ** 45%** sobre el valor de una hora normal.
Debido a buen desempeño de un empleado la empresa ocasionalmente otorga bonificaciones de 5.5% del salario base.
El salario total antes de descuentos se calcula como la suma del salario base, más el valor de las horas extras, más las bonificaciones (si las hay).
Se descontará 5% del salario total antes de descuentos para el plan obligatorio de salud.
Se descontará 4% del salario total antes de descuentos para el aporte a pensión.
Se descontará 4% del salario total antes de descuentos para caja de compensación.

Luego de considerar toda esta información, el empleado decide construir un programa que permita a cualquier empleado de la empresa verificar si los pagos son correctos.

Entrada 	
El programa recibirá 3 parámetros:
1. El salario base del empleado.
2. La cantidad de horas extras se representa a través de un número entero positivo. En caso de no realizar horas extras durante el mes, se ingresará el valor 0.
3. Si hubo bonificaciones se ingresará el valor 1, de lo contrario el valor 0.

Salida
El programa debe imprimir 2 valores:
El valor a pagar al empleado luego de realizar los descuentos de ley. El resultado debe imprimirse con un número decimal.
El salario total del empleado antes de descuentos. El resultado debe imprimirse con un número decimal.

Cada caso de prueba se especifica en una única línea.
Cada línea debe contener los valores de los parámetros separados por un espacio.
Es importante no utilizar ningún mensaje a la hora de capturar las entradas, es decir, al utilizar la función input()no agregue ningún texto para capturar los datos.
Como ejemplo considere el caso de prueba 1000000 0 0, el cual corresponde a un trabajador con una salario base de 1000000 con 0 horas extras y sin bonificaciones.
El resultado debe imprimirse con un número decimal.

Entradas de ejemplo	1000000 0 0   
                    2355255 2 1

Salida de ejemplo 	1000000.0 870000.0 
                    2519116.8 2191631.6
"""

#Entradas 
salario_base, horas_extra, bonificacion = input().split()

salario_base = float(salario_base)
horas_extra = float(horas_extra)
bonificacion = float(bonificacion)

#Proceso

valor_extras =(((salario_base/199)*horas_extra)*1.45)

#Calcula el salario total decidiendo si hay bonficacion o no



if bonificacion==1:
    salario_total =((salario_base*1.055)+valor_extras) 
else:
    salario_total =((salario_base + valor_extras))

salario_descuentos = round((salario_total * 0.87),1) 

#Salidas

print (f"""{round(salario_total,1)} {salario_descuentos}""")